﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Betting_Program
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            monthCalendar1.MaxSelectionCount = 1;
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            Program.Date = monthCalendar1.SelectionRange.Start;
            if (File.Exists("LosingDogs.csv"))
            {
                DialogResult response = MessageBox.Show(
                    "There is already a exported file, would you like to import it?",
                    "Import or Update",
                    MessageBoxButtons.YesNoCancel);
                switch (response)
                {
                    case DialogResult.Yes:
                        Program.DgTable = CsvReadWriter.Read("LosingDogs.csv");
                        dataGridView1.DataSource = Program.DgTable;
                        return;
                    case DialogResult.Cancel:
                        return;
                }
            }
            foreach (Stadium stadium in Program.DataList.StadiumsList)
            {
                stadium.Races = new List<Race>();
            }
            FileIo.GetTrackRaceInfo();
            progressBar1.Value = 33;
            FileIo.GetRaceInfo();
            progressBar1.Value = 66;
            Program.UpdateTable();
            progressBar1.Value = 100;
            btn_Export.Enabled = true;
            dataGridView1.DataSource = Program.DgTable;
        }

        private void btn_Export_Click(object sender, EventArgs e)
        {
            CsvReadWriter.Write(Program.DgTable, "LosingDogs.csv");
            MessageBox.Show("File Has Been Written");
        }

        private void btn_ShowDogStad_Click(object sender, EventArgs e)
        {
            btn_Export.Enabled = false;
            dataGridView1.DataSource = Program.DataList.StadiumDataTable;
            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);
        }

        private void btn_AddDogStad_Click(object sender, EventArgs e)
        {
            Program.Date = monthCalendar1.SelectionRange.Start;
            if (
                MessageBox.Show("Would you like to add the stadiums that are running on the day that you have selected?", "Important Question",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var stadiums = FileIo.GetRaceStadiums();
                foreach (var stadium in stadiums)
                {
                    List<String> input = Prompt.ShowDialog(System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.
    ToTitleCase(stadium.ToLower()));
                    if (input[0] != "" && input[1] != "" && input[2] != "")
                    {
                        Program.DataList.StadiumsList.Add(new Stadium(int.Parse(input[2]), input[1], input[0]));
                    }
                    else
                    {
                        MessageBox.Show("Stadium not added");
                    }
                }
            }
            else
            {
                List<string> input = Prompt.ShowDialog("");
                if (input[0] != "" && input[1] != "" && input[2] != "")
                {
                    Program.DataList.StadiumsList.Add(new Stadium(int.Parse(input[2]), input[1], input[0]));
                }
                else
                {
                    MessageBox.Show("Stadium not added");
                }
            }
            DataTable dt = Program.DataList.StadiumDataTable;
            CsvReadWriter.Write(dt, "DogStadiums.csv");
            dataGridView1.DataSource = Program.DataList.StadiumDataTable;
            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);
        }
    }
}
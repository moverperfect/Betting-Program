﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace Betting_Program
{
    internal class Stadiums
    {
        public Stadiums()
        {
            StadiumsList = new List<Stadium>();
        }

        public List<Stadium> StadiumsList { get; private set; }

        public int TotalRaces
        {
            get
            {
                return StadiumsList.Sum(stadium => stadium.TotalRaces);
            }
        }

        public DataTable StadiumDataTable
        {
            get
            {
                var dt = new DataTable();
                dt.Columns.Add("Stadium Name");
                dt.Columns.Add("Location");
                dt.Columns.Add("Stadium Id");
                foreach (var stadium in StadiumsList)
                {
                    dt.Rows.Add(new object[] {stadium.Name, stadium.Land, stadium.Id.ToString()});
                }
                return dt;
            }
        }
    }

    internal class Stadium
    {
/*
        public Stadium()
        {
            TrapWin = new List<Int16>();
            Races = new List<Race>();
        }
*/

        public Stadium(int id, String land, String name)
        {
            TrapWin = new List<Int16>();
            Races = new List<Race>();
            Name = name;
            Id = id;
            Land = land;
        }

        public List<Race> Races { get; set; }

        public List<Int16> TrapWin { get; private set; }

        public String Name { get; private set; }

        public int Id { get; private set; }

        public String Land { get; private set; }

        public int Distance { get; set; }

        public int TotalRaces { get { return Races.Count; } }

        private Int16 WorstTrap
        {
            get
            {
                Int16 b = 1;
                for (int i = 2; i < TrapWin.Count - 1; i++)
                {
                    var t = TrapWin[i];
                    if (TrapWin[b] > t)
                    {
                        b = (Int16) (i);
                    }
                }
                Int16 c = -1;
                if (TrapWin.Any(t => t - TrapWin[b] >= 3))
                {
                    c = b;
                }
                return (short) (c + 1);
            }
        }

        public IEnumerable<DataRow> GetGoodRaces()
        {
            var drList = new List<DataRow>();
            foreach (Race race in Races)
            {
                if (WorstTrap == 0) continue;
                if (race.IsGood(WorstTrap - 1).IsGood)
                {
                    var tempDr = Program.NewDataRow();
                    tempDr[0] = Name;
                    tempDr[1] = race.TimeOfDay;
                    tempDr[2] = new string(race.Classification);
                    tempDr[3] = race.IsGood(WorstTrap - 1).Name;
                    tempDr[4] = WorstTrap;
                    tempDr[5] = "http://www.racingpost.com/greyhounds/card.sd#resultDay=" +
                                Program.Date.ToString("yy-MM-dd") + "&raceId=" + race.RaceId;
                    drList.Add(tempDr);
                }
            }
            return drList;
        }
    }

    internal class Race
    {
/*
        public Race()
        {
            TimeOfDay = new TimeSpan();
            Classification = new char[1];
            Dogs = new List<Dog>();
        }
*/

        public Race(String time, String raceId, char[] classification)
        {
            TimeOfDay = new TimeSpan();
            Classification = new char[1];
            Dogs = new List<Dog>();
            TimeOfDay = TimeSpan.Parse(time);
            RaceId = raceId;
            Classification = classification;
        }

        /// <summary>
        /// The dogs that are in the race
        /// </summary>
        public List<Dog> Dogs { get; private set; }

        /// <summary>
        /// The classification of the race
        /// </summary>
        public char[] Classification { get; private set; }

        /// <summary>
        /// The time that the races is taking place at
        /// </summary>
        public TimeSpan TimeOfDay { get; private set; }

        /// <summary>
        /// The RaceID of the race
        /// </summary>
        public string RaceId { get; private set; }

        /// <summary>
        /// Gives the dog object if they are good to bet
        /// </summary>
        /// <param name="id">The id of the dog in question</param>
        /// <returns>The dog object only if they are a good bet</returns>
        public Dog IsGood(int id)
        {
            if (Dogs.Count == 0)
            { return new Dog();}
        
            if (Dogs[id].IsGood && Classification[0] == 'A' &&
                int.Parse(Classification[1].ToString(CultureInfo.InvariantCulture)) > 1 &&
                int.Parse(Classification[1].ToString(CultureInfo.InvariantCulture)) < 9)
            {
                return Dogs[id];
            }

            return new Dog();
        }
    }

    internal class Dog
    {
        public Dog()
        {
            LastPlace = 0;
        }

        public Dog(String name, int trapNo, int lastPlace)
        {
            Name = name;
            TrapNo = trapNo;
            LastPlace = lastPlace;
        }

        /// <summary>
        /// The trap number that they are in
        /// </summary>
        private int TrapNo { get; set; }

        /// <summary>
        /// The name of the dog
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The place of their last race
        /// </summary>
        private int LastPlace { get; set; }

        /// <summary>
        /// Gives true or false as to bet or not
        /// </summary>
        public bool IsGood
        {
            get { return LastPlace > 2; }
        }
    }
}
﻿namespace Betting_Program
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.btn_Update = new System.Windows.Forms.Button();
      this.progressBar1 = new System.Windows.Forms.ProgressBar();
      this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
      this.btn_Export = new System.Windows.Forms.Button();
      this.btn_ShowDogStad = new System.Windows.Forms.Button();
      this.btn_AddDogStad = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridView1
      // 
      this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Location = new System.Drawing.Point(252, 12);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new System.Drawing.Size(606, 423);
      this.dataGridView1.TabIndex = 0;
      // 
      // btn_Update
      // 
      this.btn_Update.Location = new System.Drawing.Point(13, 12);
      this.btn_Update.Name = "btn_Update";
      this.btn_Update.Size = new System.Drawing.Size(75, 23);
      this.btn_Update.TabIndex = 1;
      this.btn_Update.Text = "Update Data";
      this.btn_Update.UseVisualStyleBackColor = true;
      this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
      // 
      // progressBar1
      // 
      this.progressBar1.Location = new System.Drawing.Point(94, 12);
      this.progressBar1.Name = "progressBar1";
      this.progressBar1.Size = new System.Drawing.Size(143, 23);
      this.progressBar1.TabIndex = 2;
      // 
      // monthCalendar1
      // 
      this.monthCalendar1.Location = new System.Drawing.Point(13, 42);
      this.monthCalendar1.Name = "monthCalendar1";
      this.monthCalendar1.TabIndex = 3;
      // 
      // btn_Export
      // 
      this.btn_Export.Location = new System.Drawing.Point(12, 412);
      this.btn_Export.Name = "btn_Export";
      this.btn_Export.Size = new System.Drawing.Size(100, 23);
      this.btn_Export.TabIndex = 4;
      this.btn_Export.Text = "Export to CSV";
      this.btn_Export.UseVisualStyleBackColor = true;
      this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
      // 
      // btn_ShowDogStad
      // 
      this.btn_ShowDogStad.Location = new System.Drawing.Point(13, 217);
      this.btn_ShowDogStad.Name = "btn_ShowDogStad";
      this.btn_ShowDogStad.Size = new System.Drawing.Size(99, 36);
      this.btn_ShowDogStad.TabIndex = 5;
      this.btn_ShowDogStad.Text = "Show Dog Stadiums";
      this.btn_ShowDogStad.UseVisualStyleBackColor = true;
      this.btn_ShowDogStad.Click += new System.EventHandler(this.btn_ShowDogStad_Click);
      // 
      // btn_AddDogStad
      // 
      this.btn_AddDogStad.Location = new System.Drawing.Point(119, 217);
      this.btn_AddDogStad.Name = "btn_AddDogStad";
      this.btn_AddDogStad.Size = new System.Drawing.Size(118, 36);
      this.btn_AddDogStad.TabIndex = 6;
      this.btn_AddDogStad.Text = "Add New Dog Stadium";
      this.btn_AddDogStad.UseVisualStyleBackColor = true;
      this.btn_AddDogStad.Click += new System.EventHandler(this.btn_AddDogStad_Click);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(870, 447);
      this.Controls.Add(this.btn_AddDogStad);
      this.Controls.Add(this.btn_ShowDogStad);
      this.Controls.Add(this.btn_Export);
      this.Controls.Add(this.monthCalendar1);
      this.Controls.Add(this.progressBar1);
      this.Controls.Add(this.btn_Update);
      this.Controls.Add(this.dataGridView1);
      this.Name = "MainForm";
      this.Text = "Greyhound Picker";
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Update;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button btn_Export;
        private System.Windows.Forms.Button btn_ShowDogStad;
        private System.Windows.Forms.Button btn_AddDogStad;
    }
}


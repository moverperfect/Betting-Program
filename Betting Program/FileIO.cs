﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;
using HtmlAgilityPack;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace Betting_Program
{
    internal static class FileIo
    {
        /// <summary>
        /// Sets the trap win percentages for the inputed stadium
        /// </summary>
        /// <param name="stadium">The stadium to get the trap percentages for</param>
        private static void GetTrapData(Stadium stadium)
        {
            var wc = new WebClient();
            var doc = new HtmlDocument();
            try
            {
                doc.LoadHtml(wc.DownloadString("http://greyhound-data.com/d?page=stadia&st=" + stadium.Id + "&land=" +
                                               stadium.Land + "&stadiummode=2&distance=" + stadium.Distance));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Greyhound data currently not available, please try again later");
                return;
            }
            HtmlNode table = doc.DocumentNode.SelectNodes("//table[@id='green']")[2];

            var row = table.ChildNodes[2].ChildNodes;

            for (int i = 2; i < row.Count; i++)
            {
                var tmp = short.Parse(row[i].InnerText.TrimStart('&', 'n', 'b', 's', 'p', ';').TrimEnd('%'));
                stadium.TrapWin.Add(tmp);
            }
        }


        public static void GetTrackRaceInfo()
        {
            var wc = new WebClient();
            var doc = new HtmlDocument();
            doc.LoadHtml(
                wc.DownloadString("http://www.racingpost.com/greyhounds/greyhound_actions.sd?r_date=" +
                                  Program.Date.ToString("yy-MM-dd")));

            var left = doc.DocumentNode.SelectNodes("//div[@class='leftColumn']")[0];
            var right = doc.DocumentNode.SelectNodes("//div[@class='rightColumn']")[0];

            var stadiums = (left.ChildNodes.Count + right.ChildNodes.Count)/5;
            var races = stadiums*11;

            var totalIt = stadiums + races;
            var count = 0;

            GetEssentialRace(left,ref count, totalIt);
            GetEssentialRace(right,ref  count, totalIt);
        }

        private static void GetEssentialRace(HtmlNode column, ref int progress, int total)
        {
            for (int i = 1; i < column.ChildNodes.Count; i = i + 4)
            {
                var stadName = column.ChildNodes[i].InnerText.ToLower();
                foreach (var stadium in Program.DataList.StadiumsList)
                {
                    if (stadium.Name.ToLower() == stadName)
                    {
                        var distance =
                            column.ChildNodes[i + 2].ChildNodes[1].ChildNodes[3].InnerText.Split(')')[1].TrimStart();
                        stadium.Distance = distance.Contains("y")
                            ? Convert.ToInt32(Math.Round(float.Parse(distance.Trim('y'))*0.9144))
                            : int.Parse(distance.Trim('m'));

                        Program.MainForm.progressBar1.Value = (progress*100)/(total);
                        progress++;
                        GetTrapData(stadium);
                        Application.DoEvents();

                        var node = column.ChildNodes[i + 2];
                        for (int y = 1; y < node.ChildNodes.Count; y = y + 2)
                        {
                            var time = node.ChildNodes[y].ChildNodes[1].ChildNodes[0].InnerText;
                            var raceId =
                                node.ChildNodes[y].ChildNodes[1].ChildNodes[0].Attributes["href"].Value.Split('=')[2];
                            var classification =
                                node.ChildNodes[y].ChildNodes[3].InnerText.Split('(')[1].Split(')')[0].ToCharArray();

                            stadium.Races.Add(new Race(time, raceId, classification));
                        }
                    }
                }
            }
        }

        public static void GetRaceInfo()
        {
            var done = Program.MainForm.progressBar1.Value;
            var totalRaces = Program.DataList.TotalRaces;
            var progress = 0;

            foreach (var stadium in Program.DataList.StadiumsList)
            {
                foreach (var race in stadium.Races)
                {
                    Program.MainForm.progressBar1.Value = ((done + progress) * 100) / (totalRaces+done) - 1;
                    progress++;
                    Application.DoEvents();

                    var wc = new WebClient();
                    var doc = new HtmlDocument();
                    doc.LoadHtml(
                        wc.DownloadString("http://www.racingpost.com/greyhounds/cards_standard_card.sd?race_id=" +
                                          race.RaceId + "&r_date=" + Program.Date.ToString("yy-MM-dd")));

                    var nodes = doc.DocumentNode.SelectNodes("//table[@class='grid']");
                    if (nodes == null) break;
                    var dogs = doc.DocumentNode.SelectNodes("//div[@class='Dog']");

                    for (int i = 0; i < nodes.Count; i++)
                    {
                        var innerText = nodes[i].ChildNodes[3].ChildNodes[13].InnerText;
                        if (innerText == "" || innerText == "NR")
                        {
                            innerText = "0";
                        }
                        if (innerText.ToCharArray()[0] == '=')
                        {
                            innerText = innerText.ToCharArray()[1].ToString();
                        }
                        var lastPlace = innerText.Substring(0, 1);
                        var name =
                            dogs[i].ChildNodes[1].ChildNodes[1].ChildNodes[3].ChildNodes[1].InnerText;
                        race.Dogs.Add(new Dog(name, i + 1, int.Parse(lastPlace)));
                    }
                }
            }
        }

        internal static List<String> GetRaceStadiums()
        {
            var stadiums = new List<String>();

            var wc = new WebClient();
            var doc = new HtmlDocument();
            doc.LoadHtml(
                wc.DownloadString("http://www.racingpost.com/greyhounds/greyhound_actions.sd?r_date=" +
                                  Program.Date.ToString("yy-MM-dd")));

            var left = doc.DocumentNode.SelectNodes("//div[@class='leftColumn']")[0];
            var right = doc.DocumentNode.SelectNodes("//div[@class='rightColumn']")[0];

            bool found;

            for (int i = 1; i < left.ChildNodes.Count; i = i + 4)
            {
                found = false;
                foreach (var stadium in Program.DataList.StadiumsList)
                {
                    if (stadium.Name.ToLower() == left.ChildNodes[i].InnerText.ToLower())
                    {
                        found = true;
                    }
                }
                if (!found) { stadiums.Add(left.ChildNodes[i].InnerText);}
            }

            for (int i = 1; i < right.ChildNodes.Count; i = i + 4)
            {
                found = false;
                foreach (var stadium in Program.DataList.StadiumsList)
                {
                    if (stadium.Name.ToLower() == right.ChildNodes[i].InnerText.ToLower())
                    {
                        found = true;
                    }
                }
                if (!found) { stadiums.Add(right.ChildNodes[i].InnerText); }
            }

            return stadiums;
        } 
    }
}
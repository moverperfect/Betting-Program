﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Betting_Program
{
    public static class Prompt
    {
        public static List<String> ShowDialog(String preStad)
        {
            var prompt = new Form {Width = 500, Height = 300, Text = "New Stadium"};
            var stadNameLabel = new Label {Left = 50, Top = 20, Text = "Stadium Name"};
            var stadIdLabel = new Label {Left = 50, Top = 90, Text = "Stadium ID"};
            var stadLocLabel = new Label {Left = 50, Top = 160, Width = 300, Text = "Stadium Location(example. ie, uk)"};
            TextBox stadNameBox;
            if (preStad == "")
            {
                stadNameBox = new TextBox {Left = 50, Top = 50, Width = 400};
            }
            else
            {
                stadNameBox = new TextBox { Left = 50, Top = 50, Width = 400, Text = preStad, Enabled = false};
            }
            var stadIdBox = new TextBox {Left = 50, Top = 120, Width = 400};
            var stadLocBox = new TextBox {Left = 50, Top = 190, Width = 400};
            var confirmation = new Button {Text = "Ok", Left = 350, Width = 100, Top = 210};
            confirmation.Click += (sender, e) => prompt.Close();
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(stadNameLabel);
            prompt.Controls.Add(stadNameBox);
            prompt.Controls.Add(stadIdLabel);
            prompt.Controls.Add(stadIdBox);
            prompt.Controls.Add(stadLocLabel);
            prompt.Controls.Add(stadLocBox);
            prompt.ShowDialog();
            return new List<String> {stadNameBox.Text, stadLocBox.Text, stadIdBox.Text};
        }
    }
}
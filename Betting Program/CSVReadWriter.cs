﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Betting_Program
{
    /// <summary>
    /// Deals with all of the CSV file writing and reading
    /// </summary>
    internal static class CsvReadWriter
    {
        /// <summary>
        /// Writes all of the data inside the parsed datatable to a file with the filename passed in the string
        /// </summary>
        /// <param name="dt">The DataTable to write the data from</param>
        /// <param name="fileName">Where to write the data</param>
        public static void Write(DataTable dt, String fileName)
        {
            var sb = new StringBuilder();

            // Get all of the column names and then write them to StringBuilder
            var columnNames = dt.Columns.Cast<DataColumn>().
                Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            // For every row, write the data for all of the colunms to the StringBuilder
            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            // Write all of the text to the file
            File.WriteAllText(fileName, sb.ToString());
        }

        /// <summary>
        /// Reads all of the data into a DataTable from a specified filename
        /// </summary>
        /// <param name="fileName">The location to look for the data</param>
        /// <returns>A datatable containing all of the data in the file</returns>
        public static DataTable Read(String fileName)
        {
            var dt = new DataTable();
            var sr = new StreamReader(fileName);


            String[] col;
            try
            {
                // List of the colunms
                col = sr.ReadLine().Split(',');
            }
            catch (Exception e)
            {
                sr.Close();
                MessageBox.Show("Could not find file {0}", fileName);
                return null;
            }

            // Add the colunms
            foreach (var s in col)
            {
                dt.Columns.Add(s);
            }

            // While we are not at end add each row to the table
            while (!sr.EndOfStream)
            {
                // Split the data, get a row based off the colunms
                var data = sr.ReadLine().Split(',');
                var row = dt.NewRow();
                // Add the data to the row
                for (int i = 0; i < data.Length; i++)
                {
                    row[i] = data[i];
                }
                // Add the row to the datatable
                dt.Rows.Add(row);
            }

            // Close the reader
            sr.Close();

            return dt;
        }
    }
}
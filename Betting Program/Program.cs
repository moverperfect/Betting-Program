﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Betting_Program
{
    /// <summary>
    /// The main program class
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Hold the MainForm datagrid source
        /// </summary>
        public static DataTable DgTable = new DataTable();

        /// <summary>
        /// Hold all of the main global data
        /// </summary>
        public static Stadiums DataList = new Stadiums();

        /// <summary>
        /// The MainForm form
        /// </summary>
        public static MainForm MainForm;

        /// <summary>
        /// The DateTime that is selected in the MainForm
        /// </summary>
        public static DateTime Date;

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            // First adds all of the stadiums in the cached stadium file
            foreach (DataRow row in CsvReadWriter.Read("DogStadiums.csv").Rows)
            {
                DataList.StadiumsList.Add(new Stadium(/* The ID */int.Parse(row[2].ToString()),/* The location */ row[1].ToString(),/* The name */ row[0].ToString()));
            }

            // Initialise the datatable with the colunms for the races
            DgTable.Columns.Add("Stadium Name");
            DgTable.Columns.Add("Time of Race");
            DgTable.Columns.Add("Grade");
            DgTable.Columns.Add("Name of Dog");
            DgTable.Columns.Add("Trap No");
            DgTable.Columns.Add("Url of Race");

            // Start the mainform with the datagrid as the source and start up the form
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm = new MainForm {dataGridView1 = {DataSource = DgTable}};
            Application.Run(MainForm);
        }
        /// <summary>
        /// Return a Datarow object based on the current DataTable
        /// </summary>
        /// <returns>DataRow based on the current DataTable</returns>
        public static DataRow NewDataRow()
        {
            return DgTable.NewRow();
        }

        /// <summary>
        /// Add all of the good races in all of the stadiums in the DataList
        /// </summary>
        public static void UpdateTable()
        {
            foreach (Stadium stadium in DataList.StadiumsList)
            {
                foreach (DataRow goodRace in stadium.GetGoodRaces())
                {
                    DgTable.Rows.Add(goodRace);
                }
            }
        }
    }
}